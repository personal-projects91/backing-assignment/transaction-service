# Transactions service

Service to manage transactions

### Running the server

```
sbt run -Dhttp.port=9002
```

### API documentation

Once the service is running the [Swagger doc](http://localhost:9002/docs/swagger-ui/index.html?url=/assets/swagger.json) is available with the endpoints and schema details 

### Running test
```
sbt test
```

### Tools
- Play Framework (scala)
- sbt (plugins: sbt-scoverage - sbt-scalafmt)
