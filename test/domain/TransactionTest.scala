package domain

import java.time.Instant
import java.util.UUID

import org.scalatestplus.play.PlaySpec
import play.api.libs.json.Json

import scala.io.Source

class TransactionTest extends PlaySpec {

  "Transaction domain entity" should {

    "be able to build from json" in {
      val jsonFile = Source.fromFile("test/data/transaction-data-test.json")
      val json = Json.parse(jsonFile.mkString)
      jsonFile.close()

      val transactionList = json.as[List[Transaction]]

      transactionList.size mustBe 2

    }

    "be able to write json" in {
      val transaction = Transaction(TransactionId("a32b258f-b75d-4b85-9f06-8aa1d46438f4"),
                                    CustomerId(123),
                                    AccountId(456),
                                    Money(52.00),
                                    Instant.EPOCH)

      val jsonResult = Json.toJson(transaction)

      jsonResult
        .toString() mustBe
        """{"transactionId":"a32b258f-b75d-4b85-9f06-8aa1d46438f4","customerId":123,"accountId":456,"amount":52,"dateTime":0}"""

    }

    "build a new Transaction using functions injected" in {
      val generateUUID = () => UUID.fromString("a32b258f-b75d-4b85-9f06-8aa1d46438f4")
      val getInstant = () => Instant.ofEpochSecond(1619988755468L)
      val transaction =
        Transaction(CustomerId(123), AccountId(456), Money(52.00))(generateUUID,
                                                                   getInstant)

      transaction.transactionId.id.toString mustBe "a32b258f-b75d-4b85-9f06-8aa1d46438f4"
      transaction.dateTime.getEpochSecond mustBe 1619988755468L
    }

  }

}
