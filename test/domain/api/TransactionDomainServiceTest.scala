package domain.api

import domain.{Transaction, TransactionDomainService, TransactionHandlerError}
import domain.external.{TransactionPersistenceError, TransactionRepository}
import helpers.TestDataGen
import infrastructure.InMemoryTransactionRepository
import org.scalatest.concurrent.ScalaFutures.whenReady
import org.scalatestplus.play.PlaySpec

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class TransactionDomainServiceTest extends PlaySpec {

  "Transaction Domain Service" should {

    "handle incoming transaction" in {
      val inMemoryTransactions = new InMemoryTransactionRepository
      val transactionService = new TransactionDomainService(inMemoryTransactions)
      val result = transactionService.handleTransaction(TestDataGen.transactionSample)

      whenReady(result) { result =>
        result mustBe Right(TestDataGen.transactionSample)
      }
    }

    "map error coming from the external saving" in {
      val mockRepository = new MockErrorRepository

      val transactionService = new TransactionDomainService(mockRepository)
      val result = transactionService.handleTransaction(TestDataGen.transactionSample)

      whenReady(result) { result =>
        result mustBe Left(
          TransactionHandlerError(
            "Error returned by the transaction repository: Error saving transaction"))
      }
    }

    "get transactions by customer id" in {
      val inMemoryTransactions = new InMemoryTransactionRepository
      val transactionService = new TransactionDomainService(inMemoryTransactions)
      val result = for {
        _ <- transactionService.handleTransaction(TestDataGen.transactionSample)
        resultQuery <- transactionService.getTransactionsByCustomerId(
          TestDataGen.transactionSample.customerId)
      } yield {
        resultQuery
      }

      whenReady(result) { result =>
        result mustBe List(TestDataGen.transactionSample)
      }
    }

  }

  class MockErrorRepository extends TransactionRepository {
    override def saveTransaction(transaction: Transaction)
      : Future[Either[TransactionPersistenceError, Transaction]] =
      Future.successful(Left(TransactionPersistenceError("Error saving transaction")))

    override def readTransactionsByCustomerId(
        customerId: Long): Future[List[Transaction]] = Future.successful(List.empty)
  }

}
