package infrastructure

import helpers.TestDataGen
import org.scalatestplus.play.PlaySpec
import org.scalatest.concurrent.ScalaFutures.whenReady

class InMemoryTransactionRepositoryTest extends PlaySpec {

  "In Memory Transaction Repository" should {

    "return empty list when there is no transactions associated to a customer ID" in {
      val inMemoryTransactions = new InMemoryTransactionRepository
      val transactionFuture = inMemoryTransactions.readTransactionsByCustomerId(5555)

      whenReady(transactionFuture) { result =>
        result mustBe List.empty
      }
    }

    "save a transaction" in {
      val inMemoryTransactions = new InMemoryTransactionRepository
      val transaction = TestDataGen.transactionSample
      val transactionFuture = inMemoryTransactions.saveTransaction(transaction)

      whenReady(transactionFuture) { result =>
        result mustBe Right(transaction)
        val transactionsFuture = inMemoryTransactions.readTransactionsByCustomerId(123)
        whenReady(transactionsFuture) { transactions =>
          transactions mustBe List(transaction)
        }
      }
    }
  }

}
