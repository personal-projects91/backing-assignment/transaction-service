package helpers

import java.time.Instant

import domain._

object TestDataGen {

  val transactionSample: Transaction = {
    Transaction(
      TransactionId("a32b258f-b75d-4b85-9f06-8aa1d46438f4"),
      CustomerId(123),
      AccountId(456),
      Money(52.00),
      Instant.MIN
    )
  }
}
