package controllers

import domain.TransactionDomainService
import infrastructure.InMemoryTransactionRepository
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerTest
import play.api.test.Helpers.{
  CONTENT_TYPE,
  POST,
  contentAsString,
  contentType,
  defaultAwaitTimeout,
  status,
  stubControllerComponents
}
import play.api.test.{FakeHeaders, FakeRequest, Injecting}
import play.api.http.Status.{BAD_REQUEST, OK}
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext.Implicits.global

class TransactionControllerTest extends PlaySpec with GuiceOneAppPerTest with Injecting {
  val inMemoryTransactions = new InMemoryTransactionRepository
  val transactionService = new TransactionDomainService(inMemoryTransactions)

  "TransactionController POST /transaction" should {

    "handle new transaction and return 200" in {
      val controller =
        new TransactionController(transactionService, stubControllerComponents())

      val fakeRequest = FakeRequest(POST, "/transaction")
        .withHeaders(FakeHeaders(Seq(CONTENT_TYPE -> "application/json")))
        .withBody(Json.parse(
          """ { "customerId": 123456, "accountId": 5858, "amount": 58.78 } """))

      val response = controller.handleTransaction.apply(fakeRequest)

      status(response) mustBe OK
    }

    "return 404 for a no valid request" in {
      val controller =
        new TransactionController(transactionService, stubControllerComponents())

      val fakeRequest = FakeRequest(POST, "/transaction")
        .withHeaders(FakeHeaders(Seq(CONTENT_TYPE -> "application/json")))
        .withBody(Json.parse(
          """ { "customerId": 123456, "accountId": 5858, "amount": "58,78" } """))

      val response = controller.handleTransaction.apply(fakeRequest)

      status(response) mustBe BAD_REQUEST
    }
  }

  "TransactionController GET /transactions/customer/:id" should {

    "return list of transaction by customer" in {
      val controller =
        new TransactionController(transactionService, stubControllerComponents())
      val response =
        controller.getTransactionsByCustomersById(123456).apply(FakeRequest())

      status(response) mustBe OK
      contentType(response) mustBe Some("application/json")
      Json.parse(contentAsString(response)).result.isDefined mustBe true
    }
  }
}
