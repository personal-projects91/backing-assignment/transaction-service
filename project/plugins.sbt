addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.8")
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.4.0")
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.7.0")
addSbtPlugin("com.iheart" % "sbt-play-swagger" % "0.10.2-PLAY2.8")

