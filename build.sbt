
name := """transactions-service"""
organization := "com.moneybank"

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala, SwaggerPlugin)
scalaVersion := "2.13.5"

libraryDependencies ++= Seq(
  guice, ws, ehcache,
  "org.webjars" % "swagger-ui" % "3.43.0",
  "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test,
  "org.mockito" % "mockito-all" % "1.10.19" % Test,
  "org.scalacheck" %% "scalacheck" % "1.15.0" % Test,
  "com.typesafe.akka" %% "akka-testkit" % "2.6.14" % Test
)

coverageExcludedPackages := "<empty>;.*Reverse*;controllers.route*;controllers.GithubProxy;controllers.javascript.*;router.*;"

coverageMinimum := 80
coverageFailOnMinimum := true

swaggerDomainNameSpaces := Seq("domain")

swaggerV3 := true
