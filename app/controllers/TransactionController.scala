package controllers

import controllers.dtos.TransactionDTO
import domain.{AccountId, CustomerId, Money, Transaction}
import domain.api.TransactionService
import javax.inject.Inject
import play.api.libs.json.{JsError, JsValue, Json}
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents, Request}

import scala.concurrent.{ExecutionContext, Future}

class TransactionController @Inject()(
    val transactionService: TransactionService,
    val controllerComponents: ControllerComponents)(implicit ec: ExecutionContext)
    extends BaseController {

  def handleTransaction: Action[JsValue] =
    Action(parse.json).async { request =>
      val transactionJson = request.body.validate[TransactionDTO]
      transactionJson.fold(
        errors => {
          Future.successful(BadRequest(Json.obj("message" -> JsError.toJson(errors))))
        },
        transactionDTO => {
          val transaction = Transaction(CustomerId(transactionDTO.customerId),
                                        AccountId(transactionDTO.accountId),
                                        Money(transactionDTO.amount))()
          transactionService.handleTransaction(transaction).map {
            case Left(_)       => InternalServerError("Error handling the transaction")
            case Right(result) => Ok(Json.toJson(result))
          }
        }
      )
    }

  def getTransactionsByCustomersById(customerId: Long): Action[AnyContent] =
    Action.async { implicit request: Request[AnyContent] =>
      transactionService
        .getTransactionsByCustomerId(CustomerId(customerId))
        .map(result => Ok(Json.toJson(result)))
    }

}
