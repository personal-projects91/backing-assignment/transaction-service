package controllers.dtos

import play.api.libs.json.{Json, Reads}

case class TransactionDTO(customerId: Long, accountId: Long, amount: Double)

object TransactionDTO {
  implicit val jsonReads: Reads[TransactionDTO] = Json.reads[TransactionDTO]
}
