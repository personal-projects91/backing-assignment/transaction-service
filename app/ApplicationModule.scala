import com.google.inject.AbstractModule
import domain.TransactionDomainService
import domain.api.TransactionService
import domain.external.TransactionRepository
import infrastructure.InMemoryTransactionRepository

class ApplicationModule extends AbstractModule {
  override def configure(): Unit = {
    bind(classOf[TransactionRepository])
      .to(classOf[InMemoryTransactionRepository])

    bind(classOf[TransactionService])
      .to(classOf[TransactionDomainService])
  }
}
