package domain.external

case class TransactionPersistenceError(message: String)
