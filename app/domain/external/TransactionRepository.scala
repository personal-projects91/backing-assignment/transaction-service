package domain.external

import domain.Transaction

import scala.concurrent.Future

trait TransactionRepository {
  def saveTransaction(
      transaction: Transaction): Future[Either[TransactionPersistenceError, Transaction]]
  def readTransactionsByCustomerId(customerId: Long): Future[List[Transaction]]
}
