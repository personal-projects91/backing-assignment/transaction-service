package domain.api

import domain.{CustomerId, Transaction, TransactionHandlerError}

import scala.concurrent.{ExecutionContext, Future}

trait TransactionService {

  def getTransactionsByCustomerId(customerId: CustomerId): Future[List[Transaction]]

  def handleTransaction(transaction: Transaction)(implicit executor: ExecutionContext)
    : Future[Either[TransactionHandlerError, Transaction]]

}
