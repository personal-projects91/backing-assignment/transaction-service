package domain

import domain.api.TransactionService
import domain.external.TransactionRepository
import javax.inject.Inject
import play.api.Logging

import scala.concurrent.{ExecutionContext, Future}

class TransactionDomainService @Inject()(val transactionRepository: TransactionRepository)
    extends TransactionService
    with Logging {

  override def getTransactionsByCustomerId(
      customerId: CustomerId): Future[List[Transaction]] =
    transactionRepository.readTransactionsByCustomerId(customerId.id)

  override def handleTransaction(transaction: Transaction)(
      implicit executor: ExecutionContext)
    : Future[Either[TransactionHandlerError, Transaction]] =
    transactionRepository
      .saveTransaction(transaction)
      .map(result =>
        result.left.map(error => {
          logger.error(s"Error handling transaction: $error")
          TransactionHandlerError(
            s"Error returned by the transaction repository: ${error.message}")
        }))
}
