package domain

import java.time.Instant
import java.util.UUID

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, Reads, Writes}

case class TransactionId(id: UUID) extends AnyVal
object TransactionId {
  def apply(id: String): TransactionId = new TransactionId(UUID.fromString(id))
}
case class Money(value: Double) extends AnyVal
case class CustomerId(id: Long) extends AnyVal
case class AccountId(id: Long) extends AnyVal

case class Transaction(
    transactionId: TransactionId,
    customerId: CustomerId,
    accountId: AccountId,
    amount: Money,
    dateTime: Instant
)

object Transaction {

  def apply(customerId: CustomerId, accountId: AccountId, amount: Money)(
      randomUuid: () => UUID = () => UUID.randomUUID(),
      instantNow: () => Instant = () => Instant.now()): Transaction =
    new Transaction(
      TransactionId(randomUuid()),
      customerId,
      accountId,
      amount,
      instantNow()
    )

  private def apply(
      transactionId: String,
      customerId: Long,
      accountId: Long,
      amount: Double,
      dateTime: Instant
  ): Transaction =
    new Transaction(
      TransactionId(transactionId),
      CustomerId(customerId),
      AccountId(accountId),
      Money(amount),
      dateTime
    )

  def unapply(transaction: Transaction): (String, Long, Long, Double, Long) = (
    transaction.transactionId.id.toString,
    transaction.customerId.id,
    transaction.accountId.id,
    transaction.amount.value,
    transaction.dateTime.toEpochMilli
  )

  implicit val jsonReads: Reads[Transaction] =
    ((JsPath \ "transactionId").read[String] and (JsPath \ "customerId")
      .read[Long] and (JsPath \ "accountId").read[Long] and (JsPath \ "amount")
      .read[Double] and (JsPath \ "dateTime").read[Long])(
      (
          transactionId: String,
          customerId: Long,
          accountId: Long,
          amount: Double,
          dateTime: Long
      ) =>
        Transaction(
          transactionId,
          customerId,
          accountId,
          amount,
          Instant.ofEpochMilli(dateTime)
      )
    )

  implicit val jsonWrites: Writes[Transaction] = (
    (JsPath \ "transactionId").write[String] and
      (JsPath \ "customerId").write[Long] and
      (JsPath \ "accountId").write[Long] and
      (JsPath \ "amount").write[Double] and
      (JsPath \ "dateTime").write[Long]
  )(Transaction.unapply _)

}
