package domain

case class TransactionHandlerError(error: String)
