package infrastructure

import java.util.UUID

import domain.Transaction
import domain.external.{TransactionPersistenceError, TransactionRepository}
import javax.inject.Singleton
import play.api.Logging

import scala.collection.concurrent.Map
import scala.collection.concurrent.TrieMap
import scala.concurrent.Future
import scala.util.Try

@Singleton
class InMemoryTransactionRepository extends TransactionRepository with Logging {

  private val transactionsByIdView: Map[UUID, Transaction] = TrieMap.empty
  private val transactionByCustomerView: Map[Long, List[Transaction]] = TrieMap.empty

  override def saveTransaction(transaction: Transaction)
    : Future[Either[TransactionPersistenceError, Transaction]] = {
    Future.successful {
      Try {
        transactionsByIdView.addOne((transaction.transactionId.id, transaction))
        val transactionByCustomer =
          transactionByCustomerView.getOrElse(transaction.customerId.id, List.empty)
        transactionByCustomerView.addOne(
          (transaction.customerId.id, transactionByCustomer :+ transaction))
      } fold (error => {
        logger.error("An error occurred saving the transaction", error)
        Left(TransactionPersistenceError(error.getMessage))
      },
      _ => Right(transaction))
    }
  }

  override def readTransactionsByCustomerId(customerId: Long): Future[List[Transaction]] =
    Future.successful(transactionByCustomerView.getOrElse(customerId, List.empty))

}
